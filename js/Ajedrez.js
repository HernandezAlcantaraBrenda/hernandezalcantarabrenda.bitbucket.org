var datos = [];
var arre = [];
var conta = 0;
var Ajedrez = (function () {

    var _mostrar_tablero = function () {
        var txtFile = new XMLHttpRequest();
        txtFile.onreadystatechange = function () {
            if (this.readyState == 4) {
                //console.log(this.status)
                if (this.status == 200) {
                    datos = txtFile.responseText.split(/[|\n]+/);
                    for (var k = 0; k < 8; k++) {
                        for (var l = 0; l < 8; l++) {
                            document.getElementById(arre[conta]).innerText = datos[conta];
                            conta++;}}}}};
        txtFile.open("GET", "https://hernandezalcantarabrenda.bitbucket.io/csv/tablero.csv", true);
        txtFile.send();
        //var datos=prueba();
        var tablero = document.getElementById("tablero");
        var letras = "abcdefgh";
        var color = true;

        var conta2 = 0;
        for (var j = 0; j < 8; j++) {
            for (var i = 0; i < 8; i++) {
                var casilla = document.createElement("div");

                casilla.setAttribute("id", letras[i] + (8 - j));
                arre[conta2] = casilla.getAttribute("id");
                conta2++;
                if (color) {
                    casilla.className += " blanco";
                } else {
                    casilla.className += " negro";
                }
                document.getElementById("tablero").appendChild(casilla);
                color = !color;

            }
            color = !color;
        }
        
        document.getElementById("a8").innerText = "\u265c";
    };

    var _actualizar_tablero = function () {
        var txtFile = new XMLHttpRequest();
        txtFile.onreadystatechange = function () {
            if (this.readyState == 4) {
                //console.log(this.status)
                if (this.status == 200) {
                    datos = txtFile.responseText.split(/[|\n]+/);

                    conta = 0;
                    console.log(datos);
                    for (var k = 0; k < 8; k++) {
                        for (var l = 0; l < 8; l++) {
                            document.getElementById(arre[conta]).innerText = datos[conta];
                            conta++;
                        }
                    }
                }
            }
        };
        txtFile.open("GET","https://hernandezalcantarabrenda.bitbucket.io/csv/tablero.csv", true);
        txtFile.send();
    };
    var _mover_pieza = function (obj_parametros) {
	var letraa = obj_parametros.a.charCodeAt(0);
	var numeroa = parseInt(obj_parametros.a.charAt(1));
	
	var letrade = obj_parametros.de.charCodeAt(0);
	var numerode = parseInt(obj_parametros.de.charAt(1));
	
        if(letraa >= 97 & letraa <= 103 && numeroa >= 1 && numeroa <= 8 &&
	  letrade >= 97 & letrade <= 103 && numerode >= 1 && numerode <= 8
	  && obj_parametros.a.length == 2 && obj_parametros.de.length == 2
	) {
	var pieza1 = document.getElementById(obj_parametros.de);
        var pieza2 = document.getElementById(obj_parametros.a);
        if (pieza2.innerText == "\u2205") {
            pieza2.innerText = pieza1.innerText;
            pieza1.innerText = "\u2205";
        } else {
            alert("lugar ocupado");
        } 
	  
	} else {
	  alert("Los datos incluyen una celda no valida");
	}
    };
    return {

        "mostrarTablero": _mostrar_tablero,
        "actualizarTablero": _actualizar_tablero,
        "moverPieza": _mover_pieza
    }
})();
